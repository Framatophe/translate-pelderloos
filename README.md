# Perspectives d’avenir

### Entre la crise de la démocratie et la crise du capitalisme : quelques prévisions

*Peter Gelderloos*

Traduction (24/10/2019)&nbsp;: [Framatophe](http://christophe.masutti.name)

- Document original publié le 05/11/2018 sur [CrimethInc.](https://fr.crimethinc.com/) sur sous le titre «&nbsp;[Diagnostic of the Future. Between the Crisis of Democracy and the Crisis of Capitalism: A Forecast](https://crimethinc.com/2018/11/05/diagnostic-of-the-future-between-the-crisis-of-democracy-and-the-crisis-of-capitalism-a-forecast)&nbsp;».
- Publié aussi sur *The Anarchist Library* [à cette adresse](http://theanarchistlibrary.org/library/peter-gelderloos-diagnostic-of-the-future)

Si vous avez des remarques ou des suggestions de correction, ouvrez des issues (ne cherchez pas à commiter du texte).

# Compiler avec Pandoc

Pour obtenir un PDF / LaTeX

        pandoc  -s --latex-engine=xelatex --toc -colorlinks gelderloosdiagnostic.md -o gelderloosdiagnostic.pdf


Pour du HTML :

        pandoc -s --toc  --template template.html --css template.css gelderloosdiagnostic.md -o gelderloosdiagnostic.html


